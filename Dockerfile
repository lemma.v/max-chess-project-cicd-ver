FROM nimlang/nim:latest-alpine-onbuild
WORKDIR /chess-project
COPY . /chess-project
RUN nimble build -d:release
CMD ./chess_app
